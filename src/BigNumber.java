import sun.nio.cs.ext.MacRoman;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by dcatalans on 03/02/16.
 */
class BigNumber {
    String container;

    // Constructor 1
    public BigNumber(String s) {
        //Reemplaza desde la posicion 0 todos los 0 por un vacío.
        int i = 0;
        //Treure 0 del principi
        while(i < s.length()) {
            if (s.charAt(i) != '0') {
                break;
            }
            i++;
        }
        container = s.substring(i);
    }

    // Constructor 2
    public BigNumber(BigNumber b) {
        this.container = b.container;
    }

    // Suma
    BigNumber add(BigNumber other) {
        //Inicialitzacio de variables
        String resultado = "";
        String resultado_invertido = "";

        char[] primer_numero;
        char[] segon_numero;

        int valor;
        boolean un =false;

        //Miram el numero mes gros i el mes petit.
        if (this.compareTo(other) == 0 || this.compareTo(other) == 1) {
            primer_numero = this.container.toString().toCharArray();
            segon_numero = other.container.toString().toCharArray();
        }
        else {
            primer_numero = other.container.toString().toCharArray();
            segon_numero = this.container.toString().toCharArray();
        }

        //Utilitzam el numero mes gros per recorrer la array.
        for (int i = primer_numero.length-1, o =segon_numero.length-1; i >= 0; i--, o-- ) {
            //Si no han acabat les xifres del segon numero...
            if (o >= 0) {
                //Agafem el codi ascii i li restam 48 per tenir el numero correcte.
                valor = ((((int) primer_numero[i]) - 48) + (((int) segon_numero[o]) - 48));
            }
            else {
                valor = (int) primer_numero[i]-48;
            }
            //Si enduim 1
            if( un == true) {
                valor +=1;
                un = false;
            }
            //Si valor es major a 10, enduim 1
            if(valor >= 10) {
                valor-=10;
                un = true;
            }
            //Ficam els numeros concatenats a resultado i reiniciam la variable valor
            resultado += Integer.toString(valor);
            valor = 0;
        }
        //Tornam a comprobar si l'ultima suma en duim 1
        if (un == true) {
            resultado += "1";
        }
        //Invertim el String resultat.
        for(int x = resultado.toCharArray().length-1, j = 0; x >=0;x--, j++) {
            resultado_invertido += resultado.toCharArray()[x];
        }
        return new BigNumber(resultado_invertido);
    }

    // Resta
    BigNumber sub(BigNumber other) {
        //Inicialitzacio de variables
        String resultado ="";
        String resultado_invertido = "";

        char[] primer_numero;
        char[] segon_numero;

        int carry;
        int valor;
        boolean un =false;

        //Miram el numero mes gros i el mes petit
        if (this.compareTo(other) == 0 || this.compareTo(other) == 1) {
            primer_numero = this.container.toString().toCharArray();
            segon_numero = other.container.toString().toCharArray();
        }
        else {
            primer_numero = other.container.toString().toCharArray();
            segon_numero = this.container.toString().toCharArray();
        }

        //Utilitzam el numero mes gros per recorrer la array.
        for (int i = primer_numero.length-1, o =segon_numero.length-1; i >= 0; i--, o-- ) {
            //Miram si enduim
            if(un == true) {
                carry = 1;
                un = false;
            }
            else{
                //reiniciam la variable
                carry = 0;
            }
            //Si no han acabat les xifres del segon numero...
            if (o >= 0) {
                //Si la xifra del numero gros es menor que la xifra del numero petit...
                if ((int) primer_numero[i] < (int)segon_numero[o]+carry) {
                    valor = ((((int) primer_numero[i] +10) - 48) - (((int) segon_numero[o]) - 48 +carry));
                    un = true;
                }
                //Si les xifres son iguals..
                else if((int) primer_numero[i] == (int)segon_numero[o]+carry) {
                    valor = 0;
                }
                else {
                    valor = ((((int) primer_numero[i]) - 48) - (((int) segon_numero[o]) - 48+carry));
                }
            }
            //Si ha acabat el segon numero.
            else {
                //Si el carry (posicio de xifra del segon numero) es mayor que la xifra del numero gros
                if(carry > primer_numero[i]-48) {
                    //Sumam el numero +10 i restam el carry
                    valor = ((((int) primer_numero[i] +10) - 48) -carry);
                    //Enduim 1
                    un = true;
                }
                else {
                    valor = ((int) primer_numero[i] - 48 - carry);
                }
            }

            //Ficam els numeros concatenats a resultado
            resultado += Integer.toString(valor);
        }
        //Invertim el resultat perque doni correcte (feim operacions de dreta a esquerra)
        for(int x = resultado.toCharArray().length-1, j = 0; x >=0;x--, j++) {
            resultado_invertido += resultado.toCharArray()[x];
        }
        //retornam resultat invertit
        return new BigNumber(resultado_invertido);

    }

    // Multiplica
    BigNumber mult(BigNumber other) {
        //Inicialitzacio de variables
        //Guarda el total de la multiplicacio del primer numero
        String resultado ="";
        String resultado_invertido = "";
        //Anirà enmagatzemant els valors dels seguents numeros que multipliquen
        String segundo_resultado;
        String segundo_resultado_invertido;
        
        BigNumber valor_devolver =new BigNumber("");
        BigNumber parcial = new BigNumber("0");
        BigNumber n1;

        int contador = 0;
        int valor;
        int carry;

        char[] primer_numero;
        char[] segon_numero;

        boolean enduim = false;

        //Miram el numero mes gros i el mes petit.
        if (this.compareTo(other) == 0 || this.compareTo(other) == 1) {
            primer_numero = this.container.toString().toCharArray();
            segon_numero = other.container.toString().toCharArray();
        }
        else {
            primer_numero = other.container.toString().toCharArray();
            segon_numero = this.container.toString().toCharArray();
        }

        for (int i = segon_numero.length-1; i>=0; i--){
            //Reinitza variables.
            carry = 0;
            segundo_resultado = "";
            segundo_resultado_invertido="";

            //Controla els zeros que s'afegeixen despres de fer la primera multiplicacio amb un contador
            if(contador > 0) {
                //afegeix tants zeros con valor de contador ni ha.
                for (int cont = 0; cont < contador; cont++) {
                    segundo_resultado += "0";
                }
            }
            for (int j=primer_numero.length-1;j>=0;j--) {
                //multiplica el segon_numero[i] per cada un dels elements del numero gros
                valor = (((int) segon_numero[i] - 48) * ((int) primer_numero[j] - 48));

                //Si enduim es true, sumam el carry al valor resultant
                if(enduim){
                    valor += carry;
                    enduim = false;
                }
                //Si el valor es mayor o igual a 10, calcula tant el carry com la part que queda.
                if (valor >= 10) {
                    carry = valor / 10;
                    valor -= carry*10;
                    enduim = true;
                }
                //Si ya no es el primer pic que feim la multiplicació, guardam el resultat en segundo_resultado
                if(i != segon_numero.length-1) {
                    segundo_resultado+= Integer.toString(valor);
                }
                //Si acaba de començar la multiplicació, el primer resultat ho guardam en la variable resultado
                else {
                    resultado += Integer.toString(valor);
                }
            }
            //Afegim 1 al contador per afegir al principi els "0"
            contador ++;

            //Si el segon resultat no esta buit, fa tots els calculs.
            if (segundo_resultado != "") {
                //Si enduim qualsevol xifra
                if (enduim == true) {
                    segundo_resultado += carry;
                    enduim = false;
                }
                //Invertim el segon resultat
                for(int x = segundo_resultado.toCharArray().length-1, j = 0; x >=0;x--, j++) {
                    segundo_resultado_invertido += segundo_resultado.toCharArray()[x];
                }
                //Si ya tenim 2 numeros multiplicats
                if (contador == 2) {
                    n1 = new BigNumber(resultado_invertido);
                }
                else {
                    n1 = parcial;
                }
                //Enmagatzema el resultat de la suma en parcial.
                BigNumber n2 = new BigNumber(segundo_resultado_invertido);
                parcial = n1.add(n2);
                valor_devolver = parcial;
            }
            //Si segundo_resultado esta buit
            else {
                if(enduim == true) {
                    resultado += carry;
                    enduim = false;
                }
                //invertim resultado
                for(int x = resultado.toCharArray().length-1, j = 0; x >=0;x--, j++) {
                    resultado_invertido += resultado.toCharArray()[x];
                }
                valor_devolver = new BigNumber(resultado_invertido);
            }
        }
        //retornam valor_devolver segons si el segundo¡_numero esta buit o no.
        return valor_devolver;
    }

    // Divideix
    BigNumber div(BigNumber other) {
        String primer_numero;
        String segon_numero;
        BigNumber n1;
        BigNumber n2;
        int quocient = 0;
        String resultado = "";

        //Miram el numero mes gros
        if (this.compareTo(other) == -1) {
            resultado += new BigNumber("0");
        }
        else {
            primer_numero = this.container;
            segon_numero = other.container;

            n1 = new BigNumber(primer_numero);
            n2 = new BigNumber(segon_numero);

            while (n1.compareTo(n2) == 1 || n1.compareTo(n2) == 0) {
                n1 = n1.sub(n2);
                quocient++;
            }
            resultado += "" + quocient;
        }
        return new BigNumber(resultado);
    }

    public int compareTo(BigNumber other) {
        int res = 0;
        //Si tenen la misma longitud
        if (this.container.length() == other.container.length()) {
            //Recorr cada un dels caracters dels 2 objectes fins trobar
            // un que no sigui igual.
            for (int i = 0; i < this.container.length(); i++) {
                //Si el caracter del objecte invocador es mes petit,
                //llavors l'objecte invocador es mes petit.
                if (this.container.charAt(i) < other.container.charAt(i)) {
                    res = -1;
                    break;
                }
                //Si el caracter del objecte invocador es mes gran,
                //llavors l'objecte invocador es mes gran.
                else if (this.container.charAt(i) > other.container.charAt(i)) {
                    res = 1;
                    break;
                }
                //Si tots els caracters son iguals, llavors son exactament iguals.
                else {
                    res = 0;
                }
            }
        }
        if (this.container.length() < other.container.length()) {
            res = -1;
        }
        if (this.container.length() > other.container.length()){
            res = 1;
        }
        return res;
    }

    // Torna un String representant el número
    public String toString() {
        return container;
    }

    // Mira si dos objectes BigNumber són iguals
    public boolean equals(Object other) {
        BigNumber b = (BigNumber) other;
        if(this.container.equals(b.container)){
            return true;
        }
        else {
            return false;
        }
    }
}
